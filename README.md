# Excersize_3

this is a reference README.md file to project https://gitlab.com/alielhaggar002/java-project-jenkins

our application consists of three deployments all exposed locally throug Nodeport services.


## Containerize the application using docker.
we can do this in several ways i have included two Docker files
- Dockerfile
- Dockerfile2

build the images is as discribed in Exercise_2:

```
cd to <sevicedirectory>
mvn clean install //to generate the jar files
docker build -t alielhaggar002//productcatalogue:latest . // to build the image
docker push alielhaggar002//productcatalogue:latest' // push the image
```


## Create the docker composer file that run all the component.
services are defined in docker-compose.yml:

```
version: '3.8'

services:
  # Product Catalogue Service
  productcatalogue:
    image: alielhaggar002/productcatalogue  # Image to use for this service
    build:  # Build instructions for this service's image
      context: ./productcatalogue  # Path to the directory containing the Dockerfile for this service
      dockerfile: Dockerfile  # Name of the Dockerfile in the context directory

    ports:  # Expose ports from the container to the host machine
      - "8020:8020"  # Maps port 8020 on the host machine to port 8020 within the container

  # Shopfront Service
  shopfront:
    image: alielhaggar002/shopfront  # Image for the shopfront service
    build:  # Build instructions for the shopfront image
      context: ./shopfront  # Path to the directory containing the Dockerfile for shopfront
      dockerfile: Dockerfile  # Name of the Dockerfile in the context directory

    ports:  # Expose ports from the shopfront container
      - "8030:8030"  # Maps port 8030 on the host to port 8030 within the shopfront container

  # Stock Manager Service
  stockmanager:
    image: alielhaggar002/stockmanager  # Image for the stock manager service
    build:  # Build instructions for the stock manager image
      context: ./stockmanager  # Path to the directory containing the Dockerfile for stockmanager
      dockerfile: Dockerfile  # Name of the Dockerfile in the context directory

    ports:  # Expose ports from the stock manager container
      - "8040:8040"  # Maps port 8040 on the host to port 8040 within the stock manager container

```
This command will start the container defined in docker-compose.yml, mount my-maven-docker-project.jar into the container, and run your Maven application.
                "docker-compose up" //add the option -d to run in detached mode


## Build the deployment file needed to deploy on Kubernetes cluster

To deploy on a Kubernetes cluster, ensure you have the kubeconfig file accessible to Jenkins. Below is an example deployment.yaml file including a Deployment and a NodePort Service to expose the app locally:

deployment.yaml including deployment and service of type Node port to expose the app locally:
- if you need the app to be exposed to internet a proxy setup with a public domain is needed.
- in case of cloud ingress can be defined wth ingress resources.
Deployment files are provides in 
```
https://gitlab.com/alielhaggar002/java-project-jenkins
```
## access app:

Once deployed, you can access the application using the following URL:

http://<node-ip>:30000

Replace <node-ip> with the IP address of your Kubernetes node where the application is deployed or localhost if you are runnung minikube


***